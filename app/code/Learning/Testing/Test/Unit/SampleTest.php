<?php
 
namespace Learning\Testing\Test\Unit;
 
use Learning\Testing\TestingClass\SampleClass;
 
class SampleTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Learning\Testing\TestingClass\SampleClass
     */
    protected $sampleClass;
 
    /**
     * @var string
     */
    protected $expectedMessage;
 
    public function setUp(): void
    {
        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->sampleClass = $objectManager->getObject('Learning\Testing\TestingClass\SampleClass');
        $this->expectedMessage = 'Hello, this is sample test';
    }
 
    public function testGetMessage()
    {
        $this->assertEquals($this->expectedMessage, $this->sampleClass->getMessage());
    }
 
}